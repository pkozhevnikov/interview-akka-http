import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import cache.MapCache

import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success}

import java.util.concurrent.atomic.AtomicInteger

object Main extends App {

  implicit val system: ActorSystem[Nothing] = ActorSystem[Nothing](Behaviors.empty, "system")
  implicit val ec: ExecutionContextExecutor = system.executionContext

  val cache = new MapCache[Int, String]()

  var id = new AtomicInteger()
  implicit val nextId = () => id.incrementAndGet
  val routes = new Routes(cache)
  Http()
    .newServerAt("localhost", 3000)
    .bind(routes.route)
    .onComplete {
      case Failure(exception) =>
        system.log.error("Ошибка при запуске", exception)
      case Success(value) =>
        system.log.info("Запуск приложения")
    }
}
