import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._
import akka.stream.scaladsl._
import akka.util.ByteString
import cache.AbstractCache

import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext

import ch.megard.akka.http.cors.scaladsl.CorsDirectives._

class Routes(cache: AbstractCache[Int, String])(implicit ec: ExecutionContext, nextId: () => Int) {

  val log = org.slf4j.LoggerFactory.getLogger("Routes")

  val route: Route = 
    // format: off
    cors() {
      pathPrefix("cache") {
        pathEnd {
          delete {
            parameter("id".as[Int]) { id =>
              onComplete(cache.delete(id)) {
                case Success(true) => complete(StatusCodes.NoContent)
                case Success(false) => complete(StatusCodes.NotFound)
                case Failure(ex) =>
                  log.error(s"could not remove item [$id]", ex)
                  complete(StatusCodes.InternalServerError)
              }
            }
          } ~
          post {
            entity(as[String]) { item =>
              val id = nextId()
              onComplete(cache.write(id, item)) {
                case Success(_) => 
                  complete(HttpResponse(StatusCodes.Created, List(Location(s"/cache/$id"))))
                case Failure(ex) =>
                  log.error(s"could not write item [$item]", ex)
                  complete(StatusCodes.InternalServerError)
              }
            }
          } ~
          get {
            parameter("gt".as[Int].optional) { gto =>
              onComplete(cache.list(gto.map(gt => _ > gt))) {
                case Success(list) => 
                  complete(
                    HttpEntity(
                      ContentTypes.`text/plain(UTF-8)`,
                      Source(list).map(s => ByteString(s"$s\n"))
                    )
                  )
                case Failure(ex) =>
                  log.error("could not read items", ex)
                  complete(StatusCodes.InternalServerError)
              }
            }
          }
        } ~
        path(IntNumber) { id =>
          onComplete(cache.readById(id)) {
            case Success(Some(item)) => complete(item)
            case Success(None) => complete(StatusCodes.NotFound)
            case Failure(ex) =>
              log.error(s"could not retrieve item [$id]", ex)
              complete(StatusCodes.InternalServerError)
          }
        }
      }
    }
    // format: on
}
