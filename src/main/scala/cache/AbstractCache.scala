package cache

import scala.concurrent.Future

trait AbstractCache[Id, Item] {
  def readById(id: Id): Future[Option[Item]]
  def list(filter: Option[Id => Boolean]): Future[List[Item]]
  def write(id: Id, item: Item): Future[_]
  def delete(id: Id): Future[Boolean]
}
