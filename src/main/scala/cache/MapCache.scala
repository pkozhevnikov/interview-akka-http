package cache

import scala.collection.mutable
import scala.concurrent.{ExecutionContextExecutor, Future}

class MapCache[Id <: AnyVal, Item](implicit ec: ExecutionContextExecutor)
    extends AbstractCache[Id, Item] {
  private val map = mutable.Map[Id, Item]()

  override def readById(id: Id): Future[Option[Item]] = Future(map.get(id))

  override def list(filter: Option[Id => Boolean]): Future[List[Item]] = Future {
    filter.map(filter => map.filter(kv => filter(kv._1))).getOrElse(map).values.toList
  }

  override def write(id: Id, item: Item): Future[_] = Future(map.addOne((id, item)))

  override def delete(id: Id): Future[Boolean] = Future(map.remove(id).isDefined)
}
