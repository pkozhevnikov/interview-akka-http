import org.scalatest._
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers._
import org.mockito.ArgumentCaptor
import akka.http.scaladsl.testkit.ScalatestRouteTest

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._
import akka.stream.scaladsl._
import akka.util.ByteString

import scala.concurrent._
import scala.concurrent.duration._

import cache.AbstractCache

class RoutesSpec
    extends wordspec.AnyWordSpec
    with matchers.should.Matchers
    with ScalatestRouteTest {

  implicit val defNextId = () => 1
  import ExecutionContext.Implicits.global

  "/cache" when {

    "GET" should {

      "respond with not filtered item list" in {
        val cache = mock(classOf[AbstractCache[Int, String]])
        when(cache.list(any())).thenReturn(Future(List("abc", "cde")))
        Get("/cache") ~> new Routes(cache).route ~>
          check {
            verify(cache).list(None)
            status shouldBe StatusCodes.OK
            Await.result(
              response.entity.dataBytes
                .via(Framing.delimiter(ByteString("\n"), 256, true))
                .map(_.utf8String)
                .toMat(Sink.seq)(Keep.right)
                .run,
              1.second
            ) shouldBe Seq("abc", "cde")
          }
      }

      "respond with filtered item list" in {
        val cache = mock(classOf[AbstractCache[Int, String]])
        type Filter = Option[Int => Boolean]
        when(cache.list(any(classOf[Filter]))).thenReturn(Future(List("abc", "cde", "efg")))
        val captor = ArgumentCaptor.forClass(classOf[Filter])
        Get("/cache?gt=3") ~> new Routes(cache).route ~>
          check {
            verify(cache).list(captor.capture())
            captor.getValue.isDefined shouldBe true
            val Some(fn) = captor.getValue
            fn(2) shouldBe false
            fn(3) shouldBe false
            fn(4) shouldBe true
            fn(5) shouldBe true
            
            status shouldBe StatusCodes.OK
            Await.result(
              response.entity.dataBytes
                .via(Framing.delimiter(ByteString("\n"), 256, true))
                .map(_.utf8String)
                .toMat(Sink.seq)(Keep.right)
                .run,
              1.second
            ) shouldBe Seq("abc", "cde", "efg")
          }
      }

      "report internal failure" in {
        val cache = mock(classOf[AbstractCache[Int, String]])
        when(cache.list(any())).thenReturn(Future.failed(new Exception))
        Get("/cache") ~> new Routes(cache).route ~>
          check {
            status shouldBe StatusCodes.InternalServerError
            verify(cache).list(None)
          }
      }

    }

    "DELETE" should {

      "remove item" in {
        val cache = mock(classOf[AbstractCache[Int, String]])
        when(cache.delete(any())).thenReturn(Future(true))
        Delete("/cache?id=1") ~> new Routes(cache).route ~>
          check {
            status shouldBe StatusCodes.NoContent
            verify(cache).delete(1)
          }
      }

      "report failure" which {

        "item not found" in {
          val cache = mock(classOf[AbstractCache[Int, String]])
          when(cache.delete(any())).thenReturn(Future(false))
          Delete("/cache?id=2") ~> new Routes(cache).route ~>
            check {
              status shouldBe StatusCodes.NotFound
              verify(cache).delete(2)
            }
        }

        "internal" in {
          val cache = mock(classOf[AbstractCache[Int, String]])
          when(cache.delete(any())).thenReturn(Future.failed(new Exception))
          Delete("/cache?id=3") ~> new Routes(cache).route ~>
            check {
              status shouldBe StatusCodes.InternalServerError
              verify(cache).delete(3)
            }
        }

      }

    }

    "POST" should {

      "create item" in {
        val cache = mock(classOf[AbstractCache[Int, String]])
        when(cache.write(any(classOf[Int]), anyString())).thenAnswer(inv => {
          Future(scala.collection.mutable.Map(inv.getArguments()(0) -> inv.getArguments()(1)))
        })

        Post("/cache", "cde") ~> new Routes(cache)(ExecutionContext.global, () => 14).route ~>
          check {
            status shouldBe StatusCodes.Created
            header("Location") shouldBe Some(Location("/cache/14"))
            verify(cache).write(14, "cde")
          }
      }

      "report internal failure" in {
        val cache = mock(classOf[AbstractCache[Int, String]])
        when(cache.write(any(), any())).thenReturn(Future.failed(new Exception))
        Post("/cache", "xyz") ~> new Routes(cache)(ExecutionContext.global, () => 4).route ~>
          check {
            status shouldBe StatusCodes.InternalServerError
            verify(cache).write(4, "xyz")
          }
      }

    }

  }

  "/cache/{id}" when {

    "GET" should {

      "respond with item data" in {
        val cache = mock(classOf[AbstractCache[Int, String]])
        when(cache.readById(any())).thenReturn(Future(Some("abc")))
        Get("/cache/7") ~> new Routes(cache).route ~>
          check {
            status shouldBe StatusCodes.OK
            responseAs[String] shouldBe "abc"
            verify(cache).readById(7)
          }
      }

      "report failure" which {

        "item not found" in {
          val cache = mock(classOf[AbstractCache[Int, String]])
          when(cache.readById(any())).thenReturn(Future(None))
          Get("/cache/5") ~> new Routes(cache).route ~>
            check {
              status shouldBe StatusCodes.NotFound
              verify(cache).readById(5)
            }
        }

        "internal" in {
          val cache = mock(classOf[AbstractCache[Int, String]])
          when(cache.readById(any())).thenReturn(Future.failed(new Exception))
          Get("/cache/6") ~> new Routes(cache).route ~>
            check {
              status shouldBe StatusCodes.InternalServerError
              verify(cache).readById(6)
            }
        }

      }

    }

  }

}
